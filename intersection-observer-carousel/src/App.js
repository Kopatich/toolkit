import React from "react";
import Item from "./components/Item";
import "./styles.css";

const items = Array(10)
  .fill(0)
  .map((_, index) => ((((index + 1) * 47) % 7) + 1) * 50);

export default function App() {
  const weakIndexMap = React.useRef(new WeakMap()).current;

  const itemsMap = React.useRef(new Map()).current;

  const [selected, setSelected] = React.useState(0);

  const observerCallback = React.useCallback(
    (entriesList, observer) => {
      console.log(observer);
      entriesList.forEach(({ target, isIntersecting, intersectionRatio }) => {
        const index = weakIndexMap.get(target);

        if (typeof index !== "undefined") {
          itemsMap.set(index, { isIntersecting, intersectionRatio });
        }
      });

      const intersecting = [...itemsMap.entries()]
        .filter(([_, { isIntersecting }]) => isIntersecting)
        .sort(
          ([, { intersectionRatio: a }], [, { intersectionRatio: b }]) => b - a
        )
        .map(([index]) => index);

      console.log(intersecting);

      setSelected(intersecting[0]);
    },
    [weakIndexMap, itemsMap]
  );

  const [rootElement, setRootElement] = React.useState(undefined);

  const observer = React.useMemo(
    () =>
      new IntersectionObserver(observerCallback, {
        root: rootElement,
        threshold: Array(11)
          .fill(0)
          .map((_, index) => index * 0.1)
      }),
    [observerCallback, rootElement]
  );

  const rootRef = React.useCallback((element) => {
    setRootElement(element);
  }, []);

  React.useEffect(() => {
    const o = observer;

    return () => o.disconnect();
  }, [observer]);

  const subscribe = React.useCallback(
    (element, index) => {
      weakIndexMap.set(element, index);
      observer.observe(element);
    },
    [observer, weakIndexMap]
  );

  const unsubscribe = React.useCallback(
    (element, index) => {
      weakIndexMap.delete(element);
      observer.unobserve(element);
    },
    [observer, weakIndexMap]
  );

  return (
    <div className="App">
      <div className="Selected">selected: {selected}</div>
      <div className="Page" ref={rootRef}>
        {items.map((height, index) => (
          <Item
            height={height}
            key={index}
            index={index}
            isSelected={selected === index}
            subscribe={subscribe}
            unsubscribe={unsubscribe}
          />
        ))}
      </div>
    </div>
  );
}
