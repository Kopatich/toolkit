import React from "react";

export default function Item({
    height,
    index,
    subscribe,
    unsubscribe,
    isSelected
}) {
    const ref = React.useRef();

    React.useEffect(() => {
        const element = ref.current;
        const i = index;

        subscribe(element, i);

        const u = unsubscribe;

        return () => {
            u(element, i);
        };
    }, [subscribe, unsubscribe, index]);

    return (
        <div
            ref={ref}
            className="Item"
            style={{
                height: `${height}px`,
                background: isSelected ? "#afa" : "#fff"
            }}
        >
            {index}
        </div>
    );
}
